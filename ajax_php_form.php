<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script src="../jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
</head>
<body class='bg-dark'>
    
<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto bg-light mt-5">
            <div id='message'></div>
            <h3 class='text-center'>Ajax Registration Form</h3>
            <div>
                <label for="">Username</label>
                <input type="text" placeholder='Enter your username' class='form-control' id='username'>
                <label for="">Email</label>
                <input type="email" placeholder='Enter your email' class='form-control' id='email'>
                <label for="">Password</label>
                <input type="password" placeholder='Enter your Password' class='form-control' id='password'>
                <input type="submit" value='Register' class='btn btn-success mt-3 mb-3' id='submit_form'>
            </div>
        </div>
        <div class="col-md-6 bg-light mt-5">
            <table class='table table-bordered'>
                <thead>
                    <tr>
                        <th>S no.</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Action</th>

                    </tr>
                </thead>
                <tbody id='data_show'>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id='edit_modal'>
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Edit Data</p>
        <label for="">Username</label>
        <input type="text" id='edit_username' class='form-control'>
        <label for="">Email</label>
        <input type="text" id='edit_email' class='form-control'>
        <input type="text" id='edit_id' class='form-control' hidden>
        <button class='btn btn-info' onclick='single_user_edit()'>Update Data</button>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script>
$(function(){
    $('#submit_form').click(function(){
        let username = $('#username').val();
        let email = $('#email').val();
        let password = $('#password').val();
        $.ajax({
            url:'php_query_file.php',
            type:'post',
            data:{username:username,email:email,password:password},
            success:function(data){
                if(data == 'success'){
                    $('#message').css('background','green');
                    $('#message').html('Successfully Enter');
                    get_data();
                }
            }
        })
    });  
    
})

function get_data(){
        $.ajax({
            url:'php_query_file.php',
            type:'get',
            data:{get:''},
            success:function(data){
                $('#data_show').html(data);
            }
        })
    }
    get_data();  


function delete_data(delete_id)
{
    
    $.ajax({
        url:'php_query_file.php',
        type:'get',
        data:{delete_id},
        success:function(data)
        {
            if(data == 'successfully deleted'){
                $('#message').html('Data Deleted Successfully');
                $('#message').addClass('bg-info').addClass('text-white').addClass('text-center');
                get_data();
            }
        }
    })
}

function edit_data(edit_id)
{
    $.ajax({
        url:'php_query_file.php',
        type:'get',
        data:{edit_id},
        success:function(data){
            let new_data = JSON.parse(data);
            $('#edit_modal').modal('show');
            $('#edit_username').val(new_data['username']);
            $('#edit_email').val(new_data['email']);
            $('#edit_id').val(new_data['id']);
        }
    })
}

function single_user_edit()
{
    let update_id = $('#edit_id').val();
    let update_username = $('#edit_username').val();
    let update_email = $('#edit_email').val();
    $.ajax({
        url:'php_query_file.php',
        type:'post',
        data:{update_id:update_id,update_username:update_username,update_email:update_email},
        success:function(data){
            alert(data);
            $('#edit_modal').modal('hide');
            get_data();

        }
    })
    
}


</script>

</body>
</html>