@extends('layout.share_content')


@section('navbar')

@parent

@endsection



@section('main_content')

    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
            <form action="{{url('edit')}}" method="post">
            @csrf()
                <h3>Update Details</h3>
                <label for="">Username</label>
                <input type="hidden" value='{{$edit_data->email}}' name='update_email'>
                <input type="text" value='{{$edit_data->username}}' class='form-control' name='update_username'>
                <label for="">Address</label>
                <input type="text" value='{{$edit_data->address}}' class='form-control' name='update_address'>
                <label for="">Contact</label>
                <input type="text" value='{{$edit_data->contact}}' class='form-control' name='update_contact'>
                <input type="submit" value='Update data' class='btn btn-success'>
            </form>
            </div>
        </div>
    </div>


@endsection




@section('footer')


@parent

@endsection