@extends('layout.share_content')



@section('navbar')

@parent

@endsection



@section('main_content')
<div class="container">
    <div class="row">
    <head>
        <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    </head>
    @if(session()->has('delete_success'))
    <div class="alert alert-info">
        {{session()->get('delete_success')}}
    </div>
    @endif
        <div class="col-md-6 mx-auto  bg-dark text-white ">
            <h3 class='text-center'>Add your Details</h3>
            <form action="{{url('details_send')}}" method="post">
                @csrf()
                <label for="">Username</label>
                <input type="text" placeholder='enter username' class='form-control' name='username'>
                <label for="">Email</label>
                <input type="email" placeholder='enter email' class='form-control' name='email'>
                <label for="">Conact</label>
                <input type="number" placeholder='enter number' class='form-control' name='contact'>
                <label for="">Address</label>
                <input type="text" placeholder='enter address' class='form-control' name='address'>
                <input type="submit" value='Add Details' class='btn btn-success' >
            </form>
        </div>
    </div>
</div>
@endsection


@section('footer')

@parent

@endsection