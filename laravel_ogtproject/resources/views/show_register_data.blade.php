@extends('layout.share_content')


@section('navbar')

@parent

@endsection



@section('main_content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
        @if(session()->has('details_added'))
        <div class="alert alert-success">
            {{session()->get('details_added')}}
        </div>
        @endif
        @if(session()->has('update_data'))
        <div class="alert alert-success">
            {{session()->get('update_data')}}
        </div>
        @endif        
            <table class="table table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>S no</th>
                        <th>USERNAME</th>
                        <th>EMAIL</th>
                        <th>CONTACT</th>
                        <th>ADDRESS</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($user_details as $data)
                        <tr>
                            <td>{{$data->id}}</td>
                            <td>{{$data->username}}</td>
                            <td>{{$data->email}}</td>
                            <td>{{$data->contact}}</td>
                            <td>{{$data->address}}</td>
                            <td>
                            <a href="{{url('delete',[$data->id])}}"><button class='btn btn-danger' >DELETE</button></a>
                            <a href="{{url('edit',[$data->id])}}"><button class='btn btn-warning'>EDIT</button></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<h2>HEllo there</h2>


@endsection





@section('footer')

@parent

@endsection