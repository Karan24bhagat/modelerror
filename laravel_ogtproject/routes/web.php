<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::method('url','controller_name@fn_name');
Route::get('/','index_page@index_page_fn');  

// CRUD
Route::post('details_send','index_page@store'); // register form (create)
Route::get('show_register_data','index_page@show');  // show data (read)
Route::get('delete/{id}','index_page@delete');  // delete row (delete)
Route::get('edit/{id}','index_page@edit'); // show data of single user (edit - 1)
Route::post('edit','index_page@update_fn'); // update data of single user (edit - 2)

// END

// Route::method(get,post)
// Route::get('customurl',function(){
// return view('filename');
// })


Route::get('home','home_page@index');
// 127.0.0.1:8000/home



     



Route::get('url_test',function(){
    return 'Test url success';
});

