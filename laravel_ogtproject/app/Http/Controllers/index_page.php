<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
use App\user_details;


class index_page extends Controller
{
    public function index_page_fn()
    {
        // echo 'index controller';
        return view('index');
    }

    public function store(Request $request)
    {
        $insert_data = new user_details();
        $request_data = request()->all(); 
        $insert_data->username = $request_data['username']; 
        $insert_data->email = $request_data['email']; 
        $insert_data->contact = $request_data['contact']; 
        $insert_data->address = $request_data['address']; 
        $insert_data->save();
        // return view('index');
        session()->flash('details_added','Your Details has been added successfully');
        return redirect('show_register_data');
    }

// Session::put() , session()->flash()

    public function show()
    {
        $get_user_details_query = user_details::all();
        // echo '<pre>';
        // print_r($get_user_details_query);
        return view('show_register_data')->with('user_details',$get_user_details_query);
    }

    public function delete($id)
    {
        echo 'your id'.$id;
        $delete_user_details_query = user_details::where('id',$id)->first();
        // echo '<pre>';
        // print_r($delete_user_details_query);
        $delete_user_details_query->delete();
        // return view('index');
        session()->flash('delete_success','Your data deleted successfully');
        return redirect('/');
    }

    public function edit($id)
    {
        $get_edit_data_query = user_details::find($id);
        // echo '<pre>';
        // print_r($get_edit_data_query);
        return view('edit_page')->with('edit_data',$get_edit_data_query);  
    }
    public function update_fn(Request $request)
    {
        $table  = user_details::where('email',$request->update_email)->first();
        $table->username = $request->update_username;
        $table->address = $request->update_address;
        $table->contact = $request->update_contact;
        $table->save();
        // $show_update_data = user_details::where('email',$request->update_email)->first();
        // echo '<pre>';
        // print_r($show_update_data);
        session()->flash('update_data','Your Data is updatted successfully');
        return redirect('show_register_data');



    }

}
